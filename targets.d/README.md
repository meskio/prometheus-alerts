# Prometheus targets

This directory contains Prometheus targets. Each file should end with
`.yaml` and will be parsed as a [file_sd_config](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#file_sd_config) entry. It should
look something like this:

    # comment
    ---
    - targets:
      - example.com

Then the `targets` list is scraped by Prometheus, in accordance to the
configuration set in the `prometheus.yml` file, currently managed by
TPA inside Puppet (in the `profile::prometheus::server::external`
class).

Note that the protocol (e.g. `HTTPS`) and path suffix
(e.g. `/metrics`) are defined in the `prometheus.yml` file. In the
above example, it might mean that `https://example.com/metrics` gets
scraped. It is not possible to override those fields in the configs
here, that should be done by TPA.

This implies that each target specification (protocol and suffix)
needs a separate configuration in the Prometheus configuration. To add
a new configuration, [file a ticket with TPA](https://gitlab.torproject.org/tpo/tpa/team/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

## Current job configurations

At the time of writing, the current jobs are configured in the
`prometheus.yml` file:

    - job_name: blackbox
      metrics_path: "/probe"
      params:
        modules:
        - http_2xx
        - tcp_connect
      file_sd_configs:
      - files:
        - "/etc/prometheus-alerts/targets.d/blackbox_*.yaml"
      relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: localhost:9115
    - job_name: node
      file_sd_configs:
      - files:
        - "/etc/prometheus-alerts/targets.d/node_*.yaml"
    - job_name: bridges
      metrics_path: "/bridgestrap-metrics"
      scheme: https
      file_sd_configs:
      - files:
        - "/etc/prometheus-alerts/targets.d/bridgestrap_*.yaml"
    - job_name: rdsys
      metrics_path: "/rdsys-backend-metrics"
      scheme: https
      file_sd_configs:
      - files:
        - "/etc/prometheus-alerts/targets.d/rdsys_*.yaml"
    - job_name: snowflake-broker
      metrics_path: "/prometheus"
      scheme: https
      honor_labels: true
      file_sd_configs:
      - files:
        - "/etc/prometheus-alerts/targets.d/snowflake_*.yaml"

In other words, if you want this, you should name your target file
that:

| Purpose           | Target file pattern | Actual scrape target             |
|-------------------|---------------------|----------------------------------|
| Node exporter     | `node_*.yaml`       | `http://example.com/`            |
| Blackbox exporter | `blackbox_*.yaml`   | `http://localhost:9115/probe`    |
| Snowflake broker  | `snowflake_*.yaml`  | `https://example.com/prometheus` |

Ideally, all exporters should switch to using the standard `/metrics`
suffix so we wouldn't have to use those hacks.
