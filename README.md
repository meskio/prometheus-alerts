# Prometheus targets and alerts rules

This repository contains YAML files which describe a series of targets
(AKA "exporters") and alerting rules. The external Prometheus server
(`prometheus2.torproject.org`) will periodically fetch this repository
([between 4 and 6 hours](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/puppet/#cron-and-scheduling)), and scrape those targets for metrics,
while using the alerting rules for alerts.

Alert recipients are still configured by TPA, file a ticket with TPA
to make changes at that level.

## Alerts

Alerts live in the `rules.d/` directory, which has further documentation.

## Targets

Targets live in the `targets.d/` directory, which has further documentation.
